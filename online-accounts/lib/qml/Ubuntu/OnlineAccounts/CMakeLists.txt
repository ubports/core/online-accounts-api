project(UbuntuOnlineAccountsQML LANGUAGES CXX)

set(UOA_QML_MODULE_COMPAT UbuntuOnlineAccountsQML)

set(QML_IMPORT OnlineAccounts)
set(IMPORTS_DIR "${QT_INSTALL_QML}/Ubuntu/${QML_IMPORT}.2")
set(PLUGIN LomiriOnlineAccountsQML)
set(COMPAT_PLUGIN UbuntuOnlineAccountsQML)

add_custom_target(${UOA_QML_MODULE_COMPAT} ALL
    COMMAND ${CMAKE_COMMAND} -E create_symlink ../../Lomiri/${QML_IMPORT}/lib${PLUGIN}.so lib${COMPAT_PLUGIN}.so
)

file(GLOB QML_FILES RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} *.qml qmldir *.js)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/lib${COMPAT_PLUGIN}.so DESTINATION ${IMPORTS_DIR})
install(FILES ${QML_FILES} DESTINATION ${IMPORTS_DIR})

if(NOT ${CMAKE_CURRENT_BINARY_DIR} STREQUAL ${CMAKE_CURRENT_SOURCE_DIR})
    # copy qml files over to build dir to be able to import them uninstalled
    set(copied ${QML_FILES})
    foreach(_file ${copied})
        add_custom_command(OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/${_file}
                           DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/${_file}
                           COMMAND ${CMAKE_COMMAND} -E copy_if_different ${CMAKE_CURRENT_SOURCE_DIR}/${_file} ${CMAKE_CURRENT_BINARY_DIR}/${_file})
    endforeach(_file)
    add_custom_target(${UOA_QML_MODULE_COMPAT}_copy_compat_files_to_build_dir DEPENDS ${copied})
    add_dependencies(${UOA_QML_MODULE_COMPAT} ${UOA_QML_MODULE_COMPAT}_copy_compat_files_to_build_dir)
endif()
