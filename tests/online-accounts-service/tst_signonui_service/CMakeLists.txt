pkg_check_modules(ACCOUNTSQT accounts-qt${QT_VERSION} REQUIRED)
pkg_check_modules(APPARMOR libapparmor REQUIRED)
pkg_check_modules(SIGNONPLUGINSCOMMON signon-plugins-common REQUIRED)

include_directories(
    ${CMAKE_CURRENT_SOURCE_DIR}
    ${CMAKE_SOURCE_DIR}
    ${LomiriOnlineAccountsService_SOURCE_DIR}
    ${ACCOUNTSQT_INCLUDE_DIRS}
    ${APPARMOR_INCLUDE_DIRS}
    ${SIGNONPLUGINSCOMMON_INCLUDE_DIRS}
)

# tst_signonui_service

set(TEST tst_signonui_service)
set(SOURCES
    ${LomiriOnlineAccountsService_SOURCE_DIR}/request.cpp
    ${LomiriOnlineAccountsService_SOURCE_DIR}/request.h
    ${LomiriOnlineAccountsService_SOURCE_DIR}/signonui-service.cpp
    ${LomiriOnlineAccountsService_SOURCE_DIR}/signonui-service.h
    ${LomiriOnlineAccountsService_SOURCE_DIR}/request-manager.h
    ${LomiriOnlineAccountsService_SOURCE_DIR}/utils.cpp
    ${LomiriOnlineAccountsService_SOURCE_DIR}/utils.h
    request-manager-mock.cpp
    request-manager-mock.h
    tst_signonui_service.cpp
)

add_definitions(-DNO_REQUEST_FACTORY)

add_executable(${TEST} ${SOURCES})
target_link_libraries(${TEST}
    Qt${QT_VERSION}::Core
    Qt${QT_VERSION}::DBus
    Qt${QT_VERSION}::Network
    Qt${QT_VERSION}::Test
    ${ACCOUNTSQT_LIBRARIES}
    ${APPARMOR_LIBRARIES}
    ${SIGNONPLUGINSCOMMON_LIBRARIES}
)
add_test(${TEST} ${XVFB_COMMAND} dbus-test-runner -t ${CMAKE_CURRENT_BINARY_DIR}/${TEST})
