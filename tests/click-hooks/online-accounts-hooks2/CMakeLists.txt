set(TEST tst_online-accounts-hooks2)
set(SOURCES
    tst_online_accounts_hooks2.cpp
)

pkg_check_modules(ACCOUNTSQT accounts-qt${QT_VERSION} REQUIRED)
pkg_check_modules(SIGNONQT libsignon-qt${QT_VERSION} REQUIRED)
pkg_check_modules(QTDBUSMOCK REQUIRED libqtdbusmock-1)
pkg_check_modules(QTDBUSTEST REQUIRED libqtdbustest-1)

add_executable(${TEST} ${SOURCES})
include_directories(
    ${CMAKE_CURRENT_BINARY_DIR}
    ${ACCOUNTSQT_INCLUDE_DIRS}
    ${SIGNONQT_INCLUDE_DIRS}
    ${QTDBUSMOCK_INCLUDE_DIRS}
    ${QTDBUSTEST_INCLUDE_DIRS}
)
add_definitions(
    -DHOOK_PROCESS="${lomiri-online-accounts-hooks2_BINARY_DIR}/lomiri-online-accounts-hooks2"
    -DSIGNOND_MOCK_TEMPLATE="${CMAKE_CURRENT_SOURCE_DIR}/signond.py"
)
add_dependencies(${TEST} lomiri-online-accounts-hooks2)

target_link_libraries(${TEST}
    Qt${QT_VERSION}::Core
    Qt${QT_VERSION}::DBus
    Qt${QT_VERSION}::Xml
    Qt${QT_VERSION}::Test
    ${ACCOUNTSQT_LIBRARIES}
    ${SIGNONQT_LIBRARIES}
    ${QTDBUSMOCK_LIBRARIES}
    ${QTDBUSTEST_LIBRARIES}
)

set_target_properties(${TEST} PROPERTIES AUTOMOC TRUE)
add_test(${TEST} ${XVFB_COMMAND} dbus-test-runner -t ${CMAKE_CURRENT_BINARY_DIR}/${TEST})
