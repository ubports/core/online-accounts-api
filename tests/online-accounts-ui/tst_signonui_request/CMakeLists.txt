pkg_check_modules(SIGNONQT libsignon-qt${QT_VERSION} REQUIRED)
pkg_check_modules(ACCOUNTSQT accounts-qt${QT_VERSION} REQUIRED)
pkg_check_modules(APPARMOR libapparmor REQUIRED)
pkg_check_modules(SIGNONPLUGINSCOMMON signon-plugins-common REQUIRED)

include_directories(
    ${CMAKE_CURRENT_SOURCE_DIR}
    ${CMAKE_CURRENT_SOURCE_DIR}/..
    ${CMAKE_SOURCE_DIR}
    ${LomiriOnlineAccountsUi_SOURCE_DIR}
    ${LomiriOnlineAccountsUi_SOURCE_DIR}/../common
    ${LomiriOnlineAccountsPlugin_SOURCE_DIR}/..
    ${ACCOUNTSQT_INCLUDE_DIRS}
    ${APPARMOR_INCLUDE_DIRS}
    ${SIGNONQT_INCLUDE_DIRS}
    ${SIGNONPLUGINSCOMMON_INCLUDE_DIRS}
)

# tst_signonui_request

set(TEST tst_signonui_request)
set(SOURCES
    ${CMAKE_SOURCE_DIR}/online-accounts-common/debug.cpp
    ${LomiriOnlineAccountsUi_SOURCE_DIR}/request.h
    ${LomiriOnlineAccountsUi_SOURCE_DIR}/signonui-request.cpp
    ${LomiriOnlineAccountsUi_SOURCE_DIR}/signonui-request.h
    ${LomiriOnlineAccountsUi_SOURCE_DIR}/ui-server.h
    ../mock/request-mock.cpp
    ../mock/request-mock.h
    ../mock/qwindow.cpp
    ../mock/ui-server-mock.cpp
    ../mock/ui-server-mock.h
    window-watcher.h
    tst_signonui_request.cpp
)

add_definitions(-DNO_REQUEST_FACTORY)
add_definitions(-DTEST_DATA_DIR="${CMAKE_CURRENT_BINARY_DIR}/data")

add_executable(${TEST} ${SOURCES})
target_link_libraries(${TEST}
    Qt${QT_VERSION}::Core
    Qt${QT_VERSION}::DBus
    Qt${QT_VERSION}::Test
    lomiri-online-accounts-plugin
    ${ACCOUNTSQT_LIBRARIES}
    ${APPARMOR_LIBRARIES}
    ${SIGNONQT_LIBRARIES}
    ${SIGNONPLUGINSCOMMON_LIBRARIES}
)
add_test(${TEST} ${XVFB_COMMAND} dbus-test-runner -t ${CMAKE_CURRENT_BINARY_DIR}/${TEST})
