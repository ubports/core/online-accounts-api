include_directories(
    ${CMAKE_CURRENT_SOURCE_DIR}
    ${LomiriOnlineAccountsPlugin_SOURCE_DIR}/..
    ${ACCOUNTSQT_INCLUDE_DIRS}
    ${QTDBUSMOCK_INCLUDE_DIRS}
    ${QTDBUSTEST_INCLUDE_DIRS}
)

# tst_application_manager

set(TEST tst_application_manager)
set(SOURCES
    tst_application_manager.cpp
    ${LomiriOnlineAccountsPlugin_SOURCE_DIR}/application-manager.cpp
    ${LomiriOnlineAccountsPlugin_SOURCE_DIR}/account-manager.cpp
)

pkg_check_modules(ACCOUNTSQT accounts-qt${QT_VERSION} REQUIRED)
pkg_check_modules(QTDBUSMOCK REQUIRED libqtdbusmock-1)
pkg_check_modules(QTDBUSTEST REQUIRED libqtdbustest-1)

add_definitions(-DTEST_DATA_DIR="${CMAKE_CURRENT_BINARY_DIR}/data")

add_executable(${TEST} ${SOURCES})
target_link_libraries(${TEST}
    Qt${QT_VERSION}::Core
    Qt${QT_VERSION}::Qml
    Qt${QT_VERSION}::Test
    lomiri-online-accounts-qt${QT_VERSION}
    ${ACCOUNTSQT_LIBRARIES}
    ${QTDBUSMOCK_LIBRARIES}
    ${QTDBUSTEST_LIBRARIES}
)

add_test(${TEST} ${XVFB_COMMAND} dbus-test-runner -t ${CMAKE_CURRENT_BINARY_DIR}/${TEST})
